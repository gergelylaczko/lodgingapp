package com.example.lodgingapp.mainFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentMainBinding
import com.jakewharton.rxbinding3.view.clicks
//import com.jakewharton.rxbinding2.view.RxView
//import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.*
import io.reactivex.subjects.PublishSubject
import org.reactivestreams.Subscription

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        binding.setLifecycleOwner(this)

        /*binding.rentBtn.clicks().subscribe(view: View){
            view.findNavController().navigate(MainFragmentDirections.actionMainFragmentToRentFragment())
        }*/
        /*RxView.clicks(binding.rentBtn).subscribe { view: View ->
            view.findNavController().navigate(MainFragmentDirections.actionMainFragmentToRentFragment())
        }*/

        binding.profileBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(MainFragmentDirections.actionMainFragmentToProfileFragment())
        }

        binding.rentBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(MainFragmentDirections.actionMainFragmentToRentFragment())
        }

        binding.rentalBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(MainFragmentDirections.actionMainFragmentToRentalFragment())
        }

        try {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

        return binding.root
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }

}
