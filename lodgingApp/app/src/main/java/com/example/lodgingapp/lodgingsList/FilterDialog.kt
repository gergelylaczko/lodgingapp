package com.example.lodgingapp.lodgingsList

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentFilterDialogBinding
import com.example.lodgingapp.databinding.FragmentLodgingsListBinding
import java.lang.IllegalStateException
import java.util.*

class FilterDialog : DialogFragment() {

    internal lateinit var listener: FilterDialogListener
    private lateinit var binding: FragmentFilterDialogBinding
    private lateinit var helperBinding: FragmentLodgingsListBinding

    interface FilterDialogListener {
        fun onFilterPositiveClick(dialog: DialogFragment, city: String, minSize: Int, maxSize: Int, minRooms: Int, maxRooms: Int, minRental: Int, maxRental: Int, minOverhead: Int, maxOverhead: Int)
        fun onFilterNegativeClick(dialog: DialogFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        listener = targetFragment as FilterDialog.FilterDialogListener

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_filter_dialog, null, false)
            helperBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_lodgings_list, null, false)

            builder.setView(binding.root)
                .setPositiveButton(
                    R.string.filterPositive,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onFilterPositiveClick(this,
                            helperBinding.autoCompleteCity.text.toString(),
                            binding.minSize.text.toString().toInt(),
                            binding.maxSize.text.toString().toInt(),
                            binding.minRooms.text.toString().toInt(),
                            binding.maxRooms.text.toString().toInt(),
                            binding.minRent.text.toString().toInt(),
                            binding.maxRent.text.toString().toInt(),
                            binding.minOverhead.text.toString().toInt(),
                            binding.maxOverhead.text.toString().toInt()
                            )
                    })
                .setNegativeButton(
                    R.string.filterNegative,
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onFilterNegativeClick(this)
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null.")
    }


}
