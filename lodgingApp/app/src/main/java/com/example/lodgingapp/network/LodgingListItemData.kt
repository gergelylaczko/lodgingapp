package com.example.lodgingapp.network

import com.squareup.moshi.Json
import java.util.*

data class LodgingListItemData(
    val id: Int,
    val title: String,
    @Json(name = "monthly_rental") val monthlyRental: Int,
    @Json(name = "monthly_overhead") val monthlyOverhead: Int,
    @Json(name = "ready_to_move") val readyToMove: Date,
    val image: String,
    val city: String,
    val favourite: Boolean,
    val meeting: Boolean,
    val status: Int
)