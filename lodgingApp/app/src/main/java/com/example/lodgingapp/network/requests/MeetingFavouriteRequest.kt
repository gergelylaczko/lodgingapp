package com.example.lodgingapp.network.requests

data class MeetingFavouriteRequest(
    val lodgingId: Int
)