package com.example.lodgingapp.network

import com.squareup.moshi.Json

class ProfileData(
    val id: Int,
    val email: String,
    @Json(name = "firstName") val firstName: String,
    @Json(name = "lastName") val lastName: String,
    @Json(name = "phone") val phone: String,
    @Json(name = "avatar") val image:String
)