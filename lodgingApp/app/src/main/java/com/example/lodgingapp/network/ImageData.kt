package com.example.lodgingapp.network

data class ImageData(
    val id: Int,
    val path: String
)