package com.example.lodgingapp.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun formatDate(date: Date?): String {
        if (date != null) {
            return SimpleDateFormat("yyyy. MM. dd.").format(date)
        }
        return ""
    }
}