package com.example.lodgingapp.favoritesList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FavoriteListItemBinding
import com.example.lodgingapp.network.FavoriteListItemData
import com.example.lodgingapp.utils.DateUtils

class FavoritesListAdapter(val clickListener: FavoritesListItemListener) :
    ListAdapter<FavoriteListItemData, FavoritesListAdapter.ViewHolder>(FavoriteListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data, clickListener)
    }

    class ViewHolder private constructor(val binding: FavoriteListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: FavoriteListItemData, clickListener: FavoritesListItemListener) {
            binding.itemData = data
            binding.clickListener = clickListener
            binding.dateUtils = DateUtils

            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FavoriteListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class FavoriteListItemDiffCallback : DiffUtil.ItemCallback<FavoriteListItemData>() {

    override fun areContentsTheSame(oldItem: FavoriteListItemData, newItem: FavoriteListItemData): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: FavoriteListItemData, newItem: FavoriteListItemData): Boolean {
        return oldItem.id == newItem.id
    }
}

class FavoritesListItemListener(val clickListener: (id: Int, title: String) -> Unit) {
    fun onClick(data: FavoriteListItemData) = clickListener(data.id, data.title)
}
