package com.example.lodgingapp.contactFragment

import android.content.Intent
import android.net.Uri
import android.nfc.FormatException
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentContactBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ContactFragment : Fragment() {

    private lateinit var binding: FragmentContactBinding
    private lateinit var viewModel: ContactViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this).get(ContactViewModel::class.java)

        binding.send.observableClickListener().subscribe {
            if (showAllViewError()) {
                sendEmail("serfozomarcell@gmail.com")
                Toast.makeText(context, resources.getString(R.string.missingTextToast), Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, resources.getString(R.string.wrongEmail), Toast.LENGTH_LONG).show()
            }
        }

        return binding.root
    }

    private fun sendEmail(email: String) {
        if (viewModel.checkEmail(email))
            makeEmail(email)
        else
            throw FormatException()
    }

    private fun makeEmail(email: String) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:$email")
        intent.putExtra(Intent.EXTRA_SUBJECT, binding.subject.text.toString())
        intent.putExtra(Intent.EXTRA_TEXT, binding.editText.text.toString())
        startActivity(intent)
    }

    private fun showAllViewError(): Boolean {
        return if (checkOneViewError(binding.subject) && checkOneViewError(binding.editText)) {
            true
        } else {
            showOneViewError(binding.subject)
            showOneViewError(binding.editText)

            false
        }
    }

    private fun showOneViewError(view: TextView) {
        if (TextUtils.isEmpty(view.text))
            view.error = resources.getString(R.string.missingText)
    }

    private fun checkOneViewError(view: TextView): Boolean {
        return !TextUtils.isEmpty(view.text)
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }
}
