package com.example.lodgingapp.myLodgingsList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodgingapp.databinding.MyLodgingListItemBinding
import com.example.lodgingapp.network.MyLodgingsData
import com.example.lodgingapp.utils.DateUtils

class MyLodgingsListAdapter(val clickListener: MyLodgingsListItemListener) :
    ListAdapter<MyLodgingsData, MyLodgingsListAdapter.ViewHolder>(MyLodgingsListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data, clickListener)
    }

    class ViewHolder private constructor(val binding: MyLodgingListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: MyLodgingsData, clickListener: MyLodgingsListItemListener) {
            binding.itemData = data
            binding.clickListener = clickListener
            binding.dateUtils = DateUtils

            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MyLodgingListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class MyLodgingsListItemDiffCallback : DiffUtil.ItemCallback<MyLodgingsData>() {

    override fun areContentsTheSame(oldItem: MyLodgingsData, newItem: MyLodgingsData): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: MyLodgingsData, newItem: MyLodgingsData): Boolean {
        return oldItem.id == newItem.id
    }
}

class MyLodgingsListItemListener(val clickListener: (id: Int, title: String) -> Unit) {
    fun onClick(data: MyLodgingsData) = clickListener(data.id, data.title)
}
