package com.example.lodgingapp.network

import com.example.lodgingapp.network.requests.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*

private const val BASE_URL = "http://192.168.1.107:3000"
private const val token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImVtYWlsIjoibGFsaUB0ZXN6dC5jb20iLCJpYXQiOjE1NjY0NTkxNTgsImV4cCI6MTU2NjU0NTU1OH0.2i3q2tEoQ9AkOh_mfk_imOeq2J4YQQTNi24-bVycdPM"

enum class ApiStatus { LOADING, ERROR, DONE }

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .add(Date::class.java, Rfc3339DateJsonAdapter())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface ApiService {
    @Headers("Authorization: $token")
    @GET("lodging/list/{city}")
    fun listLodgings(@Path("city") cityName: String): Deferred<List<LodgingListItemData>>

    @Headers("Authorization: $token")
    @GET("lodging/cities")
    fun listCities(): Deferred<List<CityData>>

    @Headers("Authorization: $token")
    @GET("lodging/details/{id}")
    fun getLodgingDetails(@Path("id") lodgingId: Int): Deferred<LodgingData>

    @Headers("Authorization: $token")
    @POST("meetings/add")
    fun addMeeting(@Body body: MeetingFavouriteRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("favourites/add")
    fun addFavourite(@Body body: MeetingFavouriteRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("meetings/delete")
    fun deleteMeeting(@Body body: MeetingFavouriteRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("favourites/remove")
    fun deleteFavourite(@Body body: MeetingFavouriteRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("create/add")
    fun addLodging(@Body body: AddLodgingRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @GET("user/profile")
    fun getProfile(): Deferred<ProfileData>

    @Headers("Authorization: $token")
    @POST("user/update")
    fun updateProfile(@Body body: ProfileUpdateRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @GET("favourites/list")
    fun listFavorites(): Deferred<List<FavoriteListItemData>>

    @Headers("Authorization: $token")
    @GET("meetings/list")
    fun listMeetings(): Deferred<List<MeetingListItemData>>

    @Headers("Authorization: $token")
    @GET("lodging/own")
    fun listMyLodgings(): Deferred<List<MyLodgingsData>>

    @Headers("Authorization: $token")
    @POST("edit/delete")
    fun deleteLodging(@Body body: DeleteLodgingRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("create/update")
    fun updateLodging(@Body body: UpdateLodgingRequest): Deferred<SimpleStatusResponse>

    @Headers("Authorization: $token")
    @POST("lodging/filter")
    fun filter(@Body body: FilterRequest): Deferred<SimpleStatusResponse>
}

object Api {
    val retrofitService: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}


