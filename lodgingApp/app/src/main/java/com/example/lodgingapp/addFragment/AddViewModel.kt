package com.example.lodgingapp.addFragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.CityData
import com.example.lodgingapp.network.requests.AddLodgingRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AddViewModel : ViewModel() {

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _cities = MutableLiveData<List<CityData>>()
    val cities: LiveData<List<CityData>>
        get() = _cities

    val cityNames = Transformations.map(cities) {
        it.map { it.city }
    }

    init {
        getCities()
    }

    fun getAutoCompleteText() {
        coroutineScope.launch {
            try {
                _status.value = ApiStatus.LOADING
                _status.value = ApiStatus.DONE
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
            }
        }
    }

    private fun getCities() {
        coroutineScope.launch {
            val getCitiesDeferred = Api.retrofitService.listCities()
            try {
                _status.value = ApiStatus.LOADING
                val citiesResult = getCitiesDeferred.await()
                _status.value = ApiStatus.DONE
                _cities.value = citiesResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _cities.value = ArrayList()
            }
        }
    }

    fun addLodging(lodging: AddLodgingRequest) {
        coroutineScope.launch {
            val getResponseDeferred = Api.retrofitService.addLodging(lodging)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
            } catch (e: Exception) {
                Log.i("lodgingAdd", e.message)
                _status.value = ApiStatus.ERROR
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}