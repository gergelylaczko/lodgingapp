package com.example.lodgingapp.network.requests

data class DeleteLodgingRequest(
    val lodgingId: Int
)