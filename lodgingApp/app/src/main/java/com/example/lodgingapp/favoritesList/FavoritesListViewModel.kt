package com.example.lodgingapp.favoritesList

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.FavoriteListItemData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FavoritesListViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _list = MutableLiveData<List<FavoriteListItemData>>()
    val list: LiveData<List<FavoriteListItemData>>
        get() = _list

    fun getLodgingsList() {
        coroutineScope.launch {
            val getListDeferred = Api.retrofitService.listFavorites()
            try {
                _status.value = ApiStatus.LOADING
                val listResult = getListDeferred.await()
                _status.value = ApiStatus.DONE
                _list.value = listResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _list.value = ArrayList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}