package com.example.lodgingapp.lodgingsList


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentLodgingsListBinding
import com.example.lodgingapp.network.CityData


class LodgingsListFragment : Fragment() {

    private lateinit var viewModel: LodgingsListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: FragmentLodgingsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_lodgings_list, container, false)
        binding.lifecycleOwner = this

        viewModel = ViewModelProviders.of(this).get(LodgingsListViewModel::class.java)

        val adapter = LodgingListAdapter(ListItemListener { id, title ->
            this.findNavController()
                .navigate(LodgingsListFragmentDirections.actionLodgingsListFragmentToLodgingDetailsFragment(id, title))
        })
        binding.lodgingList.adapter = adapter

        viewModel.list.observe(this, Observer {
            if (it !== null) {
                adapter.submitList(it)
            }
        })

        viewModel.cityNames.observe(this, Observer {
            if (it !== null) {
                ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, it).also { adapter ->
                    binding.autoCompleteCity.setAdapter(adapter)
                }
            }
        })

        binding.autoCompleteCity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.getLodgingsList(p0.toString())
            }
        })

        binding.autoCompleteCity.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                hideKeyboard()
            }
        })

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.list_menu, menu)
    }

    private fun hideKeyboard() {
        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }
}
