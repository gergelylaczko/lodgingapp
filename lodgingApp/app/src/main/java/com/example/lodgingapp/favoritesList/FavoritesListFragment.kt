package com.example.lodgingapp.favoritesList

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentFavoritesListBinding

class FavoritesListFragment : Fragment() {

    private lateinit var viewModel: FavoritesListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentFavoritesListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites_list, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this).get(FavoritesListViewModel::class.java)

        val adapter = FavoritesListAdapter(FavoritesListItemListener { id, title ->
            this.findNavController().navigate(FavoritesListFragmentDirections.actionFavoritesListFragmentToLodgingDetailsFragment(id, title))
        })

        binding.lodgingList.adapter = adapter

        viewModel.list.observe(this, Observer {
            if (it !== null) {
                adapter.submitList(it)
            }
        })

        viewModel.getLodgingsList()

        return binding.root
    }

}
