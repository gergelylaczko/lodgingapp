package com.example.lodgingapp.network

import com.squareup.moshi.Json

data class MyLodgingsData(
    val id: Int,
    val title: String,
    @Json(name = "monthly_rental") val monthlyRental: Int,
    @Json(name = "image") val image: String,
    val city: String,
    val status: String
)