package com.example.lodgingapp.animationActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.lodgingapp.MainActivity
import com.example.lodgingapp.R

class AnimationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animation)

        Handler().postDelayed({
            startActivity(Intent(this@AnimationActivity, MainActivity::class.java))
            finish()
        }, 330)
    }
}
