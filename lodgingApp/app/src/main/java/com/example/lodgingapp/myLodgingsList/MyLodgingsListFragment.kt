package com.example.lodgingapp.myLodgingsList

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentMyLodgingsListBinding
import com.example.lodgingapp.databinding.MyLodgingListItemBinding

class MyLodgingsListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentMyLodgingsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_lodgings_list, container, false)
        binding.lifecycleOwner = this
        val viewModel = ViewModelProviders.of(this).get(MyLodgingsListViewModel::class.java)

        val adapter = MyLodgingsListAdapter(MyLodgingsListItemListener { id, title ->
            this.findNavController().navigate(MyLodgingsListFragmentDirections.actionMyLodgingsListFragmentToLodgingEditFragment(id, title))
        })

        binding.lodgingList.adapter = adapter

        viewModel.list.observe(this, Observer {
            if (it !== null) {
                adapter.submitList(it)
            }
        })

        viewModel.getLodgingsList()

        return binding.root
    }
}
