package com.example.lodgingapp.network

import com.squareup.moshi.Json
import java.util.*

data class MeetingListItemData(
    val id: Int,
    val title: String,
    @Json(name = "monthly_rental") val monthlyRental: Int,
    @Json(name = "caution_money") val cautionMoney: Int,
    @Json(name = "saved_at") val savedAt: Date,
    @Json(name = "main_image") val image: String
)