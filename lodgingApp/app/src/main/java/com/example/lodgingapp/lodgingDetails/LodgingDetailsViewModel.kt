package com.example.lodgingapp.lodgingDetails

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.LodgingData
import com.example.lodgingapp.network.requests.MeetingFavouriteRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class LodgingDetailsViewModel(private val lodgingId: Int) : ViewModel() {

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _details = MutableLiveData<LodgingData>()
    val details: LiveData<LodgingData>
        get() = _details


    init {
        getLodgingDetails()
    }

    fun getLodgingDetails() {
        coroutineScope.launch {
            val getDetailsDeferred = Api.retrofitService.getLodgingDetails(lodgingId)
            try {
                _status.value = ApiStatus.LOADING
                val detailResult = getDetailsDeferred.await()
                _status.value = ApiStatus.DONE
                _details.value = detailResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _details.value = null
            }
        }
    }

    fun toggleFavourite() {
        if (details.value!!.favourite) {
            deleteFavoutire()
        } else {
            addFavoutire()
        }
    }

    fun toggleMeeting() {
        if (details.value!!.meeting) {
            deleteMeeting()
        } else {
            addMeeting()
        }
    }

    private fun addFavoutire() {
        coroutineScope.launch {
            val body = MeetingFavouriteRequest(details.value!!.id)
            val getResponseDeferred = Api.retrofitService.addFavourite(body)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
                getLodgingDetails()
            } catch (e: Exception) {
                Log.i("details_vm", e.message)
                _status.value = ApiStatus.ERROR
            }
        }
    }

    private fun deleteFavoutire() {
        coroutineScope.launch {
            val body = MeetingFavouriteRequest(details.value!!.id)
            val getResponseDeferred = Api.retrofitService.deleteFavourite(body)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
                getLodgingDetails()
            } catch (e: Exception) {
                Log.i("details_vm", e.message)
                _status.value = ApiStatus.ERROR
            }
        }
    }


    private fun addMeeting() {
        coroutineScope.launch {
            val body = MeetingFavouriteRequest(details.value!!.id)
            val getResponseDeferred = Api.retrofitService.addMeeting(body)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
                getLodgingDetails()
            } catch (e: Exception) {
                Log.i("details_vm", e.message)
                _status.value = ApiStatus.ERROR
            }
        }
    }

    private fun deleteMeeting() {
        coroutineScope.launch {
            val body = MeetingFavouriteRequest(details.value!!.id)
            val getResponseDeferred = Api.retrofitService.deleteMeeting(body)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
                getLodgingDetails()
            } catch (e: Exception) {
                Log.i("details_vm", e.message)
                _status.value = ApiStatus.ERROR
            }
        }
    }
}