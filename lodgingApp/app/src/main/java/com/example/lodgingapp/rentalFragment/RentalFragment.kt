package com.example.lodgingapp.rentalFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentRentalBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RentalFragment : Fragment() {

    private lateinit var binding: FragmentRentalBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rental, container, false)
        binding.lifecycleOwner = this

        binding.addBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(RentalFragmentDirections.actionRentalFragmentToAddFragment())
        }

        binding.lodgingListBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(RentalFragmentDirections.actionRentalFragmentToLodgingsListFragment())
        }

        binding.changeServiceBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigateUp()
        }

        binding.myAddsBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(RentalFragmentDirections.actionRentalFragmentToMyLodgingsListFragment())
        }

        try {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

        return binding.root
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }

}
