package com.example.lodgingapp.meetingsList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentMeetingsListBinding

class MeetingsListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentMeetingsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_meetings_list, container, false)
        binding.lifecycleOwner = this
        val viewModel = ViewModelProviders.of(this).get(MeetingsListViewModel::class.java)

        val adapter = MeetingsListAdapter(MeetingsListItemListener { id, title ->
            this.findNavController().navigate(MeetingsListFragmentDirections.actionMeetingsListFragmentToLodgingDetailsFragment(id, title))
        })

        binding.lodgingList.adapter = adapter

        viewModel.list.observe(this, Observer {
            if (it !== null) {
                adapter.submitList(it)
            }
        })

        viewModel.getLodgingsList()

        return binding.root
    }


}
