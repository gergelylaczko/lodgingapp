package com.example.lodgingapp.meetingsList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodgingapp.databinding.MeetingListItemBinding
import com.example.lodgingapp.network.MeetingListItemData
import com.example.lodgingapp.utils.DateUtils

class MeetingsListAdapter(val clickListener: MeetingsListItemListener) :
    ListAdapter<MeetingListItemData, MeetingsListAdapter.ViewHolder>(MeetingsListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data, clickListener)
    }

    class ViewHolder private constructor(val binding: MeetingListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: MeetingListItemData, clickListener: MeetingsListItemListener) {
            binding.itemData = data
            binding.clickListener = clickListener
            binding.dateUtils = DateUtils

            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MeetingListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class MeetingsListItemDiffCallback : DiffUtil.ItemCallback<MeetingListItemData>() {

    override fun areContentsTheSame(oldItem: MeetingListItemData, newItem: MeetingListItemData): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: MeetingListItemData, newItem: MeetingListItemData): Boolean {
        return oldItem.id == newItem.id
    }
}

class MeetingsListItemListener(val clickListener: (id: Int, title: String) -> Unit) {
    fun onClick(data: MeetingListItemData) = clickListener(data.id, data.title)
}
