package com.example.lodgingapp.addFragment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.component1
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.navigation.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentAddBinding
import com.example.lodgingapp.network.requests.AddLodgingRequest
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.lang.Exception
import java.util.*
import java.text.SimpleDateFormat

class AddFragment : Fragment() {

    private lateinit var binding: FragmentAddBinding
    private lateinit var viewModel: AddViewModel
    lateinit var newLodging: AddLodgingRequest

    @SuppressLint("CheckResult", "ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this).get(AddViewModel::class.java)

        binding.editText.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        binding.moveable.observableClickListener().subscribe { view: View ->
            val dialog = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                binding.moveable.setText(String.format(getString(R.string.datepicker_text), year, month + 1, day))
            }, year, month, day)

            dialog.show()

            view.hideKeyboard()
        }

        viewModel.cityNames.observe(this, Observer {
            if (it !== null) {
                ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, it).also { adapter ->
                    binding.city.setAdapter(adapter)
                }
            }
        })

        binding.city.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.getAutoCompleteText()
            }
        })

        binding.save.observableClickListener().subscribe { view: View ->
            try {
                showAllViewError()
                addLodgingToDatabase()
                Toast.makeText(this.context, resources.getString(R.string.successfulSave), Toast.LENGTH_LONG).show()

                //át kell irányítani a 'Hirdetéseim' fragmentre
                view.findNavController().navigate(AddFragmentDirections.actionAddFragmentToLodgingsListFragment())
                view.hideKeyboard()
            } catch (e: Exception) {
                Toast.makeText(this.context, resources.getString(R.string.missingTextToast), Toast.LENGTH_LONG).show()
            }
        }

        return binding.root
    }

    private fun addLodgingToDatabase() {
        //region supportProps
        val title = binding.title.text.toString()
        val size = binding.area.text.toString().toInt()
        val rooms = binding.rooms.text.toString().toInt()
        val monthlyRental = binding.rentPerMonth.text.toString().toInt()
        val monthlyOverhead = binding.overhead.text.toString().toInt()
        val cautionMoney = binding.caution.text.toString().toInt()
        val readyToMove = binding.moveable.text.toString()
        val minRentalTime = binding.minRentTime.text.toString().toInt()
        val description = binding.editText.text.toString()
        val city = binding.city.text.toString()
        val street = binding.street.text.toString()
        val houseNumber = binding.houseNum.text.toString().toInt()
        val typeId = binding.type.selectedItemPosition
        val conditionId = binding.state.selectedItemPosition
        val elevator = binding.lift.selectedItemPosition
        val heatingId = binding.heating.selectedItemPosition
        val furniture = binding.furnished.selectedItemPosition
        val balcony = binding.balcony.selectedItemPosition
        val yard = binding.court.selectedItemPosition
        val pet = binding.pet.selectedItemPosition
        val smoking = binding.smoke.selectedItemPosition
        val parkingId = binding.parking.selectedItemPosition

        val floor: Int?
        if (TextUtils.isEmpty(binding.floor.text))
            floor = null
        else
            floor = binding.floor.text.toString().toInt()
        val door: Int?
        if (TextUtils.isEmpty(binding.door.text))
            door = null
        else
            door = binding.door.text.toString().toInt()
        //endregion
        //region initNewLodging
        newLodging = AddLodgingRequest(
            title = title,
            size = size,
            rooms = rooms,
            monthlyRental = monthlyRental,
            monthlyOverhead = monthlyOverhead,
            cautionMoney = cautionMoney,
            readyToMove = readyToMove,
            minRentalTime = minRentalTime,
            description = description,
            city = city,
            street = street,
            houseNumber = houseNumber,
            floor = floor,
            door = door,
            typeId = typeId,
            conditionId = conditionId,
            elevator = elevator,
            heatingId = heatingId,
            furniture = furniture,
            balcony = balcony,
            yard = yard,
            pet = pet,
            smoking = smoking,
            parkingId = parkingId
        )
        //endregion

        viewModel.addLodging(newLodging)
    }

    private fun showAllViewError() {
        showOneViewError(binding.title)
        showOneViewError(binding.area)
        showOneViewError(binding.rooms)
        showOneViewError(binding.rentPerMonth)
        showOneViewError(binding.overhead)
        showOneViewError(binding.caution)
        showOneViewError(binding.moveable)
        showOneViewError(binding.minRentTime)
        showOneViewError(binding.editText)
        showOneViewError(binding.city)
        showOneViewError(binding.street)
        showOneViewError(binding.houseNum)
    }

    private fun showOneViewError(view: TextView) {
        if (TextUtils.isEmpty(view.text))
            view.error = resources.getString(R.string.missingText)
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }

}
