package com.example.lodgingapp.lodgingsList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.LodgingListItemBinding
import com.example.lodgingapp.network.LodgingListItemData
import com.example.lodgingapp.utils.DateUtils

class LodgingListAdapter(val clickListener: ListItemListener) :
    ListAdapter<LodgingListItemData, LodgingListAdapter.ViewHolder>(ListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data, clickListener)
    }

    class ViewHolder private constructor(val binding: LodgingListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: LodgingListItemData, clickListener: ListItemListener) {
            binding.itemData = data
            binding.clickListener = clickListener
            binding.dateUtils = DateUtils

            setFavouriteIcon(data)
            setMeetingIcon(data)
            setStatusBar(data)

            binding.executePendingBindings()
        }

        private fun setFavouriteIcon(data: LodgingListItemData) {
            if (data.favourite) {
                binding.favouriteIcon.visibility = View.VISIBLE
            } else {
                binding.favouriteIcon.visibility = View.GONE
            }
        }

        private fun setMeetingIcon(data: LodgingListItemData) {
            if (data.meeting) {
                binding.meetingIcon.visibility = View.VISIBLE
            } else {
                binding.meetingIcon.visibility = View.GONE
            }
        }

        private fun setStatusBar(data: LodgingListItemData) {
            if (data.status == 1) {
                binding.lodgingState.setBackgroundResource(R.drawable.lodging_list_item_status_active)
            } else {
                binding.lodgingState.setBackgroundResource(R.drawable.lodging_list_item_status_rented)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LodgingListItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class ListItemDiffCallback : DiffUtil.ItemCallback<LodgingListItemData>() {

    override fun areContentsTheSame(oldItem: LodgingListItemData, newItem: LodgingListItemData): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: LodgingListItemData, newItem: LodgingListItemData): Boolean {
        return oldItem.id == newItem.id
    }
}

class ListItemListener(val clickListener: (id: Int, title: String) -> Unit) {
    fun onClick(data: LodgingListItemData) = clickListener(data.id, data.title)
}
