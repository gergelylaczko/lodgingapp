package com.example.lodgingapp.network.requests

data class FilterRequest(
    val city: String,
    val minSize: Int,
    val maxSize: Int,
    val minRooms: Int,
    val maxRooms: Int,
    val minRental: Int,
    val maxRental: Int,
    val minOverhead: Int,
    val maxOverhead: Int
)