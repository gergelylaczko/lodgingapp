package com.example.lodgingapp.network.requests

import java.util.*

data class AddLodgingRequest(
    val title: String,
    val size: Int,
    val rooms: Int,
    val monthlyRental: Int,
    val monthlyOverhead: Int,
    val cautionMoney: Int,
    val readyToMove: String,
    val minRentalTime: Int,
    val city: String,
    val street: String,
    val houseNumber: Int,
    val floor: Int?,
    val door: Int?,
    val description: String,
    val typeId: Int,
    val conditionId: Int,
    val elevator: Int,
    val heatingId: Int,
    val furniture: Int,
    val balcony: Int,
    val yard: Int,
    val pet: Int,
    val smoking: Int,
    val parkingId: Int
)