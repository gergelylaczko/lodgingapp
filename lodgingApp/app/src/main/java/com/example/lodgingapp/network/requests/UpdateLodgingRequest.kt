package com.example.lodgingapp.network.requests

import java.util.*

data class UpdateLodgingRequest(
    val lodgingId: Int,
    val city: String,
    val title: String,
    val size: Int,
    val rooms: Int,
    val monthlyRental: Int,
    val monthlyOverhead: Int,
    val cautionMoney: Int,
    val readyToMove: Date,
    val minRentalTime: Int,
    val street: String,
    val houseNumber: Int,
    val description: String,
    val typeId: Int,
    val conditionId: Int,
    val elevator: Int,
    val heatingId: Int,
    val furniture: Int,
    val balcony: Int,
    val yard: Int,
    val pet: Int,
    val smoking: Int,
    val parkingId: Int
)