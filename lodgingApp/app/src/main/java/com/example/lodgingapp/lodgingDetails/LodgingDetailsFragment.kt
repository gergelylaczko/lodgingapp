package com.example.lodgingapp.lodgingDetails

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentLodgingDetailsBinding
import com.example.lodgingapp.utils.DateUtils

class LodgingDetailsFragment : Fragment() {

    private lateinit var viewModel: LodgingDetailsViewModel
    private lateinit var binding: FragmentLodgingDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lodging_details, container, false)
        binding.lifecycleOwner = this

        val arguments = LodgingDetailsFragmentArgs.fromBundle(arguments!!)
        val viewModelFactory = LodgingDetailsViewModelFactory(arguments.lodgingId)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LodgingDetailsViewModel::class.java)

        binding.viewModel = viewModel
        binding.dateUtils = DateUtils

        val adapter = ImageListAdapter()
        binding.imageList.adapter = adapter

        viewModel.details.observe(this, Observer {
            if (it != null) {
                adapter.submitList(it.images)
            }
        })

        try {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

        return binding.root
    }


}
