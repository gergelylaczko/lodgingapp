package com.example.lodgingapp.rentFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentRentBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RentFragment : Fragment() {

    private lateinit var binding: FragmentRentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rent, container, false)
        binding.lifecycleOwner = this

        binding.lodgingListBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(RentFragmentDirections.actionRentFragmentToLodgingsListFragment())
        }

        binding.changeServiceBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigateUp()
        }

        binding.favoritesBtn.observableClickListener().subscribe { view: View ->
            view.findNavController().navigate(RentFragmentDirections.actionRentFragmentToFavoritesListFragment())
        }

        binding.meetingsBtn.observableClickListener().subscribe{ view:View->
            view.findNavController().navigate(RentFragmentDirections.actionRentFragmentToMeetingsListFragment())
        }

        try {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

        return binding.root
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }

}
