package com.example.lodgingapp.lodgingEdit

import android.util.Log
import android.widget.Button
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.LodgingData
import com.example.lodgingapp.network.requests.DeleteLodgingRequest
import com.example.lodgingapp.network.requests.UpdateLodgingRequest
import kotlinx.android.synthetic.main.fragment_lodging_edit.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class LodgingEditViewModel(private val lodgingId: Int) : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _details = MutableLiveData<LodgingData>()
    val details: LiveData<LodgingData>
        get() = _details

    init {
        getLodgingDetails()
    }

    fun getLodgingDetails() {
        coroutineScope.launch {
            val getDetailsDeferred = Api.retrofitService.getLodgingDetails(lodgingId)
            try {
                _status.value = ApiStatus.LOADING
                val detailResult = getDetailsDeferred.await()
                _status.value = ApiStatus.DONE
                _details.value = detailResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _details.value = null
            }
        }
    }

    fun deleteLodging() {
        coroutineScope.launch {
            val body = DeleteLodgingRequest(lodgingId)
            val getResponseDeferred = Api.retrofitService.deleteLodging(body)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
            }
        }
    }

    //region spinners
    fun typeSpinnerItem(s: Spinner) {
        val index = (when {
            details.value?.type.toString() == "Tégla" -> 1
            details.value?.type.toString() == "Panel" -> 2
            details.value?.type.toString() == "Egyéb" -> 3
            else -> 0
        })
        s.post { s.setSelection(index) }
    }

    fun stateSpinnerItem(s: Spinner) {
        val index = (when {
            details.value?.conditionType.toString() == "Felújított" -> 1
            details.value?.conditionType.toString() == "Jó" -> 2
            details.value?.conditionType.toString() == "Átlagos" -> 3
            details.value?.conditionType.toString() == "Felújítandó" -> 4
            else -> 0
        })
        s.post { s.setSelection(index) }
    }

    fun heatingSpinnerItem(s: Spinner) {
        val index = (when {
            details.value?.heating.toString() == "Gázkazán" -> 1
            details.value?.heating.toString() == "Geotermikus" -> 2
            details.value?.heating.toString() == "Házközponti" -> 3
            details.value?.heating.toString() == "Elektromos" -> 4
            details.value?.heating.toString() == "Távfűtés" -> 5
            details.value?.heating.toString() == "Egyéb" -> 6
            else -> 0
        })
        s.post { s.setSelection(index) }
    }

    fun parkingSpinnerItem(s: Spinner) {
        val index = (when {
            details.value?.parking.toString() == "Nincs" -> 1
            details.value?.parking.toString() == "Utcán" -> 2
            details.value?.parking.toString() == "Udvaron" -> 3
            details.value?.parking.toString() == "Garázs" -> 4
            details.value?.parking.toString() == "Egyéb" -> 5
            else -> 0
        })
        s.post { s.setSelection(index) }
    }
    //endregion

    fun updateLodging(lodging: UpdateLodgingRequest) {
        coroutineScope.launch {
            val getResponseDeferred = Api.retrofitService.updateLodging(lodging)
            try {
                _status.value = ApiStatus.LOADING
                val response = getResponseDeferred.await()
                _status.value = ApiStatus.DONE
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
            }
        }
    }
}