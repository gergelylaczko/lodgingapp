package com.example.lodgingapp.profileFragment

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentProfileEditBinding
import com.example.lodgingapp.network.requests.ProfileUpdateRequest
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ProfileEditFragment : Fragment() {

    private lateinit var binding: FragmentProfileEditBinding
    private lateinit var viewModel: ProfileEditViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_edit, container, false)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this).get(ProfileEditViewModel::class.java)
        binding.viewModel = viewModel

        binding.save.observableClickListener().subscribe { view: View ->
            if (showAllViewError()) {
                update()
                Toast.makeText(this.context, resources.getString(R.string.successfulSave), Toast.LENGTH_LONG).show()
                view.findNavController().navigateUp()
            } else {
                Toast.makeText(this.context, resources.getString(R.string.missingTextToast), Toast.LENGTH_LONG).show()
            }
        }

        return binding.root
    }

    private fun update() {
        val newDetails = ProfileUpdateRequest(
            binding.email.text.toString(),
            binding.firstName.text.toString(),
            binding.lastName.text.toString(),
            binding.phoneNumber.text.toString()
        )

        viewModel.updateProfile(newDetails)
    }

    private fun showAllViewError(): Boolean {
        return if (checkOneViewError(binding.lastName) && checkOneViewError(binding.firstName) &&
            checkOneViewError(binding.email) && checkOneViewError(binding.phoneNumber)
        ) {
            true
        } else {
            showOneViewError(binding.lastName)
            showOneViewError(binding.firstName)
            showOneViewError(binding.email)
            showOneViewError(binding.phoneNumber)

            false
        }
    }

    private fun showOneViewError(view: TextView) {
        if (TextUtils.isEmpty(view.text))
            view.error = resources.getString(R.string.missingText)
    }

    private fun checkOneViewError(view: TextView): Boolean {
        return !TextUtils.isEmpty(view.text)
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }

}
