package com.example.lodgingapp.network

data class SimpleStatusResponse(
    val message: String
)