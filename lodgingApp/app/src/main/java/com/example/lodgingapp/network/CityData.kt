package com.example.lodgingapp.network

import com.squareup.moshi.Json

data class CityData(
    val id: Int,
    @Json(name = "name") val city: String
)
