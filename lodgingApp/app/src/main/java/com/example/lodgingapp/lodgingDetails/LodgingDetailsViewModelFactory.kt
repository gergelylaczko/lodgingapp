package com.example.lodgingapp.lodgingDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class LodgingDetailsViewModelFactory(private val lodgingId: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LodgingDetailsViewModel::class.java)) {
            return LodgingDetailsViewModel(lodgingId) as T
        }
        throw IllegalArgumentException("Unknown view model class.")
    }

}