package com.example.lodgingapp.network.requests

data class ProfileUpdateRequest (
    val email: String,
    val firstName: String,
    val lastName: String,
    val phone: String
)