package com.example.lodgingapp.lodgingsList

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.CityData
import com.example.lodgingapp.network.LodgingListItemData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class LodgingsListViewModel : ViewModel() {

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _list = MutableLiveData<List<LodgingListItemData>>()
    val list: LiveData<List<LodgingListItemData>>
        get() = _list

    private val _cities = MutableLiveData<List<CityData>>()
    val cities: LiveData<List<CityData>>
        get() = _cities

    val cityNames = Transformations.map(cities) {
        it.map { it.city }
    }

    init {
        getCities()
    }

    fun getLodgingsList(cityName: String) {
        coroutineScope.launch {
            val getListDeferred = Api.retrofitService.listLodgings(cityName)
            try {
                _status.value = ApiStatus.LOADING
                val listResult = getListDeferred.await()
                _status.value = ApiStatus.DONE
                _list.value = listResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _list.value = ArrayList()
            }
        }
    }

    private fun getCities() {
        coroutineScope.launch {
            val getCitiesDeferred = Api.retrofitService.listCities()
            try {
                _status.value = ApiStatus.LOADING
                val citiesResult = getCitiesDeferred.await()
                _status.value = ApiStatus.DONE
                _cities.value = citiesResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _cities.value = ArrayList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}