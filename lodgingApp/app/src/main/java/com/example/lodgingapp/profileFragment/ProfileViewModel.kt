package com.example.lodgingapp.profileFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lodgingapp.network.Api
import com.example.lodgingapp.network.ApiStatus
import com.example.lodgingapp.network.ProfileData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ProfileViewModel: ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _details = MutableLiveData<ProfileData>()
    val details: LiveData<ProfileData>
        get() = _details


    init {
        getLodgingDetails()
    }

    fun getLodgingDetails() {
        coroutineScope.launch {
            val getDetailsDeferred = Api.retrofitService.getProfile()
            try {
                _status.value = ApiStatus.LOADING
                val detailResult = getDetailsDeferred.await()
                _status.value = ApiStatus.DONE
                _details.value = detailResult
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _details.value = null
            }
        }
    }
}