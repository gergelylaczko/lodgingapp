package com.example.lodgingapp.network

import com.squareup.moshi.Json
import com.squareup.moshi.Types
import java.lang.reflect.Type
import java.util.*
import com.squareup.moshi.JsonAdapter

data class LodgingData(
    val id: Int,
    val title: String,
    val size: Int,
    val rooms: Int,
    @Json(name = "monthly_rental") val monthlyRental: Int,
    @Json(name = "monthly_overhead") val monthlyOverhead: Int,
    @Json(name = "caution_money") val cautionMoney: Int,
    @Json(name = "ready_to_move") val readyToMove: Date,
    @Json(name = "min_rental_time") val minRentalTime: Int,
    val city: String,
    val street: String,
    @Json(name = "house_number") val houseNumber: Int,
    val floor: Int?,
    val door: Int?,
    val description: String,
    val type: String,
    @Json(name = "condition_type") val conditionType: String,
    val elevator: Int,
    val heating: String,
    val furniture: Int,
    val balcony: Int,
    val yard: Int,
    val pet: Int,
    val smoking: Int,
    val parking: String,
    val date: Date,
    val owner: Boolean,
    val email: String,
    @Json(name = "first_name") val firstName: String,
    @Json(name = "last_name") val lastName: String,
    val phone: String,
    val status: String,
    val favourite: Boolean,
    val meeting: Boolean,
    val images: List<ImageData>
)