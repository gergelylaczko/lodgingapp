package com.example.lodgingapp.impressumFragment


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentImpressumBinding

class ImpressumFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentImpressumBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_impressum, container, false)
        binding.setLifecycleOwner(this)

        binding.webview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }

        binding.webview.loadUrl("file:///android_asset/impressumHtml.html")

        try {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
        }

        return binding.root
    }


}
