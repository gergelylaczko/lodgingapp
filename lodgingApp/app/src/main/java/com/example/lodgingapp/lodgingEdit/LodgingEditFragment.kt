package com.example.lodgingapp.lodgingEdit

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController

import com.example.lodgingapp.R
import com.example.lodgingapp.databinding.FragmentLodgingEditBinding
import com.example.lodgingapp.lodgingDetails.ImageListAdapter
import com.example.lodgingapp.lodgingDetails.LodgingDetailsViewModel
import com.example.lodgingapp.network.requests.UpdateLodgingRequest
import com.example.lodgingapp.utils.DateUtils
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


class LodgingEditFragment : Fragment() {

    private lateinit var binding: FragmentLodgingEditBinding
    private lateinit var viewModel: LodgingEditViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lodging_edit, container, false)
        binding.lifecycleOwner = this
        val arguments = LodgingEditFragmentArgs.fromBundle(arguments!!)
        val viewModelFactory = LodgingEditViewModelFactory(arguments.lodgingId)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LodgingEditViewModel::class.java)
        binding.viewModel = viewModel
        binding.dateUtils = DateUtils

        val adapter = ImageListAdapter()
        binding.imageList.adapter = adapter

        viewModel.details.observe(this, Observer {
            if (it != null) {
                adapter.submitList(it.images)
            }
        })

        binding.deleteBtn.observableClickListener().subscribe { view: View ->
            val builder = AlertDialog.Builder(this.context!!)
            builder.setTitle(resources.getString(R.string.confirmDeleteText))
            builder.setMessage("")
            builder.setPositiveButton(resources.getString(R.string.yes)) { dialog, id ->
                viewModel.deleteLodging()
                view.findNavController().navigateUp()
            }
            builder.setNegativeButton(resources.getString(R.string.no)) { dialog, id ->

            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        binding.moveable.observableClickListener().subscribe { view: View ->
            val dialog = DatePickerDialog(this.context!!, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                binding.moveable.setText(String.format(getString(R.string.datepicker_text), year, month + 1, day))
            }, year, month, day)

            dialog.show()

            view.hideKeyboard()
        }

        //viewModel.setSpinnerItem(binding.type)
        //binding.button.text=viewModel.details.value?.type.toString()

        binding.loadDatasBtn.observableClickListener().subscribe {
            viewModel.typeSpinnerItem(binding.type)
            viewModel.stateSpinnerItem(binding.state)
            viewModel.details.value?.elevator?.let { binding.lift.setSelection(it) }
            viewModel.heatingSpinnerItem(binding.heating)
            viewModel.details.value?.furniture?.let { binding.furnished.setSelection(it) }
            viewModel.details.value?.balcony?.let { binding.balcony.setSelection(it) }
            viewModel.details.value?.yard?.let { binding.court.setSelection(it) }
            viewModel.details.value?.pet?.let { binding.pet.setSelection(it) }
            viewModel.details.value?.smoking?.let { binding.smoke.setSelection(it) }
            viewModel.parkingSpinnerItem(binding.parking)
        }

        binding.save.setOnClickListener{view: View ->
            updateLodging()
            view.findNavController().navigateUp()
        }

        return binding.root
    }

    @SuppressLint("SimpleDateFormat")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun updateLodging() {
        //region supportProps
        val splittedDate = binding.moveable.text.toString().split('-')
        val year = splittedDate[0]
        val month = if(splittedDate[1].length == 1) "0${splittedDate[1]}" else splittedDate[1]
        val day = splittedDate[2].toInt()

        val defaultZoneId = ZoneId.systemDefault()
        val ld = LocalDate.parse("$year-$month-$day", DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        val date = Date.from(ld.atStartOfDay(defaultZoneId).toInstant())

        val title = if(TextUtils.isEmpty(binding.title.text.toString())) viewModel.details.value!!.title else binding.title.text.toString()
        val area = if(TextUtils.isEmpty(binding.area.text.toString())) viewModel.details.value!!.size else binding.area.text.toString().toInt()
        val rooms = if(TextUtils.isEmpty(binding.rooms.text.toString())) viewModel.details.value!!.rooms else binding.rooms.text.toString().toInt()
        val rentPerMonth = if(TextUtils.isEmpty(binding.rentPerMonth.text.toString())) viewModel.details.value!!.monthlyRental else binding.rentPerMonth.text.toString().toInt()
        val overhead = if(TextUtils.isEmpty(binding.overhead.text.toString())) viewModel.details.value!!.monthlyOverhead else binding.overhead.text.toString().toInt()
        val caution = if(TextUtils.isEmpty(binding.caution.text.toString())) viewModel.details.value!!.cautionMoney else binding.caution.text.toString().toInt()
        val minRentTime = if(TextUtils.isEmpty(binding.minRentTime.text.toString())) viewModel.details.value!!.minRentalTime else binding.minRentTime.text.toString().toInt()
        val description = if(TextUtils.isEmpty(binding.description.text.toString())) viewModel.details.value!!.description else binding.description.text.toString()

        val type = binding.type.selectedItemPosition
        val state = binding.state.selectedItemPosition
        val lift = binding.lift.selectedItemPosition
        val heating = binding.heating.selectedItemPosition
        val furnished = binding.furnished.selectedItemPosition
        val balcony = binding.balcony.selectedItemPosition
        val court = binding.court.selectedItemPosition
        val pet = binding.pet.selectedItemPosition
        val smoke = binding.smoke.selectedItemPosition
        val parking = binding.parking.selectedItemPosition
        //endregion
        //region initLodging
        val lodging = UpdateLodgingRequest(
            lodgingId = viewModel.details.value!!.id,
            city = viewModel.details.value!!.city,
            title = title,
            size = area,
            rooms = rooms,
            monthlyRental = rentPerMonth,
            monthlyOverhead = overhead,
            cautionMoney = caution,
            readyToMove = date,
            minRentalTime = minRentTime,
            street = viewModel.details.value!!.street,
            houseNumber = viewModel.details.value!!.houseNumber,
            description = description,
            typeId = type,
            conditionId = state,
            elevator = lift,
            heatingId = heating,
            furniture = furnished,
            balcony = balcony,
            yard = court,
            pet = pet,
            smoking = smoke,
            parkingId = parking
        )
        //endregion
        viewModel.updateLodging(lodging)
    }

    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun View.observableClickListener(): Observable<View> {
        val publishSubject: PublishSubject<View> = PublishSubject.create()
        this.setOnClickListener { v -> publishSubject.onNext(v) }
        return publishSubject
    }
}
