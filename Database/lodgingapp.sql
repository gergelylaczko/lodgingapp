-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1:3306
-- Létrehozás ideje: 2019. Aug 02. 11:03
-- Kiszolgáló verziója: 5.7.26
-- PHP verzió: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `lodgingapp`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Budapest'),
(2, 'Eger');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `conditions`
--

DROP TABLE IF EXISTS `conditions`;
CREATE TABLE IF NOT EXISTS `conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `conditions`
--

INSERT INTO `conditions` (`id`, `name`) VALUES
(1, 'Felújított'),
(2, 'Jó'),
(3, 'Átlagos'),
(4, 'Felújítandó');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `favourites`
--

DROP TABLE IF EXISTS `favourites`;
CREATE TABLE IF NOT EXISTS `favourites` (
  `uid` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`,`apartment_id`),
  KEY `FK_fav_lodgings` (`apartment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `favourites`
--

INSERT INTO `favourites` (`uid`, `apartment_id`, `date`) VALUES
(1, 1, '2019-07-30 09:25:17');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `heating_types`
--

DROP TABLE IF EXISTS `heating_types`;
CREATE TABLE IF NOT EXISTS `heating_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `heating_types`
--

INSERT INTO `heating_types` (`id`, `name`) VALUES
(1, 'Gázkazán'),
(2, 'Geotermikus'),
(3, 'Házközponti'),
(4, 'Elektromos'),
(5, 'Távfűtés'),
(6, 'Egyéb');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `lodgings`
--

DROP TABLE IF EXISTS `lodgings`;
CREATE TABLE IF NOT EXISTS `lodgings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `size` mediumint(8) UNSIGNED NOT NULL,
  `rooms` smallint(5) UNSIGNED NOT NULL,
  `monthly_rental` int(10) UNSIGNED NOT NULL,
  `monthly_overhead` int(10) UNSIGNED NOT NULL,
  `caution_money` int(10) UNSIGNED NOT NULL,
  `min_rental_time` tinyint(3) UNSIGNED NOT NULL,
  `city` int(11) NOT NULL,
  `street` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `house_number` smallint(5) UNSIGNED NOT NULL,
  `floor` smallint(6) DEFAULT NULL,
  `door` tinyint(4) DEFAULT NULL,
  `description` text COLLATE utf8_hungarian_ci NOT NULL,
  `type` int(11) NOT NULL,
  `condition_type` int(11) NOT NULL,
  `elevator` tinyint(1) NOT NULL,
  `heating` int(11) NOT NULL,
  `furniture` tinyint(1) NOT NULL,
  `balcony` tinyint(1) NOT NULL,
  `yard` tinyint(1) NOT NULL,
  `pet` tinyint(1) NOT NULL,
  `smoking` tinyint(1) NOT NULL,
  `parking` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `uid` int(11) NOT NULL,
  `status` int(11) DEFAULT '1',
  `main_image` varchar(250) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `ready_to_move` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `FK_lodgings_types` (`type`),
  KEY `FK_lodgings_cities` (`city`),
  KEY `FK_lodgings_conditions` (`condition_type`),
  KEY `FK-lodgings_heatings` (`heating`),
  KEY `FK_lodgings_parking` (`parking`),
  KEY `FK_lodgings_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `lodgings`
--

INSERT INTO `lodgings` (`id`, `title`, `size`, `rooms`, `monthly_rental`, `monthly_overhead`, `caution_money`, `min_rental_time`, `city`, `street`, `house_number`, `floor`, `door`, `description`, `type`, `condition_type`, `elevator`, `heating`, `furniture`, `balcony`, `yard`, `pet`, `smoking`, `parking`, `date`, `uid`, `status`, `main_image`, `ready_to_move`) VALUES
(1, 'Teszt 1.', 50, 2, 60000, 15000, 100000, 2, 2, 'Teszt utca', 160, 4, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et mi eu urna rhoncus volutpat et sit amet enim. Fusce elementum dictum nisi id feugiat. Integer mattis molestie vehicula. Vestibulum placerat lobortis lorem, vel faucibus purus blandit id. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas ullamcorper lectus mi, sed laoreet lacus egestas eu. Phasellus massa nulla, bibendum eu ultricies a, fermentum sit amet purus. Duis suscipit mollis odio, vitae pulvinar nunc finibus id.\r\n\r\n', 2, 3, 0, 1, 1, 1, 0, 0, 0, 2, '2019-07-29 14:39:32', 1, 1, 'default-image.png', '2019-08-02'),
(3, 'Teszt 2.', 40, 2, 50000, 10000, 80000, 1, 2, 'Teszt utca.', 65, NULL, NULL, 'sfdg srwwsreag reag aed aergase rdgsaer easrg erg eagr', 2, 4, 0, 2, 0, 1, 0, 1, 1, 2, '2019-07-30 13:27:18', 1, 1, 'default-image.png', '2019-08-01'),
(60, 'Teszt 2. Renamed', 60, 2, 60000, 20000, 100000, 2, 2, 'Teszt utca', 8, NULL, NULL, 'Description', 2, 1, 0, 2, 1, 1, 0, 1, 0, 2, '2019-08-01 10:57:11', 1, 1, '2019-08-01T08-57-11.679Zlogo.png', '2019-09-10');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `meetings`
--

DROP TABLE IF EXISTS `meetings`;
CREATE TABLE IF NOT EXISTS `meetings` (
  `uid` int(11) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`,`apartment_id`),
  KEY `FK_meetings_lodgings` (`apartment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `meetings`
--

INSERT INTO `meetings` (`uid`, `apartment_id`, `date`) VALUES
(1, 60, '2019-08-01 13:49:19');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `parking_types`
--

DROP TABLE IF EXISTS `parking_types`;
CREATE TABLE IF NOT EXISTS `parking_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `parking_types`
--

INSERT INTO `parking_types` (`id`, `name`) VALUES
(1, 'Nincs'),
(2, 'Utcán'),
(3, 'Udvaron'),
(4, 'Garázs'),
(5, 'Egyéb');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lodging_id` int(11) NOT NULL,
  `path` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_photos_lodgings` (`lodging_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `photos`
--

INSERT INTO `photos` (`id`, `lodging_id`, `path`) VALUES
(75, 60, '2019-08-01T08-57-11.677Zdefault-avatar.png'),
(76, 60, '2019-08-01T08-57-11.679Zlogo.png');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rent_status`
--

DROP TABLE IF EXISTS `rent_status`;
CREATE TABLE IF NOT EXISTS `rent_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `rent_status`
--

INSERT INTO `rent_status` (`id`, `name`) VALUES
(1, 'Aktív'),
(2, 'Kiadva');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `types`
--

INSERT INTO `types` (`id`, `type`) VALUES
(1, 'Tégla'),
(2, 'Panel'),
(3, 'Egyéb');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `first_name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `avatar` varchar(250) COLLATE utf8_hungarian_ci DEFAULT 'default-avatar.png',
  `password` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`uid`, `email`, `first_name`, `last_name`, `phone`, `avatar`, `password`) VALUES
(1, 'teszt@teszt.com', 'Teszt', 'Teszt', '12345678', 'default-avatar.png', '$2a$10$/ogqgY.nFNEyKMR9axAKZ.wV5rcjjWqQ3yrKeox8j5OgUrbaQnGgS'),
(3, 'teszt', 'teszt', 'teszt', '1234', 'default-avatar.png', '$2a$10$HcGYnnEHaqHlL883OHMjPeCfzebIQWfkvnXSvrY1sKTweMvKNzENm');

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `FK_fav_lodgings` FOREIGN KEY (`apartment_id`) REFERENCES `lodgings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_fav_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE;

--
-- Megkötések a táblához `lodgings`
--
ALTER TABLE `lodgings`
  ADD CONSTRAINT `FK-lodgings_heatings` FOREIGN KEY (`heating`) REFERENCES `heating_types` (`id`),
  ADD CONSTRAINT `FK_lodgings_cities` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `FK_lodgings_conditions` FOREIGN KEY (`condition_type`) REFERENCES `conditions` (`id`),
  ADD CONSTRAINT `FK_lodgings_parking` FOREIGN KEY (`parking`) REFERENCES `parking_types` (`id`),
  ADD CONSTRAINT `FK_lodgings_status` FOREIGN KEY (`status`) REFERENCES `rent_status` (`id`),
  ADD CONSTRAINT `FK_lodgings_types` FOREIGN KEY (`type`) REFERENCES `types` (`id`),
  ADD CONSTRAINT `FK_lodgings_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE;

--
-- Megkötések a táblához `meetings`
--
ALTER TABLE `meetings`
  ADD CONSTRAINT `FK_meetings_lodgings` FOREIGN KEY (`apartment_id`) REFERENCES `lodgings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_meetings_users` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`) ON DELETE CASCADE;

--
-- Megkötések a táblához `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `FK_photos_lodgings` FOREIGN KEY (`lodging_id`) REFERENCES `lodgings` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
