const connection = require('../database/database');
const defaultAvatar = require('../config.json').defaultAvatar;

const avatarPath = (fileName) => `uploads/avatars/${fileName}`;



exports.userDetails = (req, res, next) => {
    const uid = req.userData.uid;
    connection.query('SELECT uid AS id, email, first_name AS firstName, last_name AS lastName, phone, avatar FROM users WHERE uid = ?',
        [uid], function (err, result) {
            res.status(200).json({ ...result[0] });
        });
};


exports.updateUser = (req, res, next) => {
    const avatar = req.file ? req.file.filename : null;
    const { email, firstName, lastName, phone } = req.body;
    const uid = req.userData.uid;

    const sql = 'UPDATE users SET email = ?, first_name = ?, last_name = ?, phone = ? WHERE uid = ?';
    connection.query(sql, [email, firstName, lastName, phone, uid], function (err, result) {
        try {
            if (err) throw err;
            if (avatar) {
                changeAvatar(uid, avatar);
            }
            res.status(201).json({ message: 'User data updated.' });
        } catch (err) {
            next(err);
        }
    });

};


const changeAvatar = (uid, avatar) => {
    connection.query('SELECT avatar FROM users WHERE uid = ?', [uid], function (err, result) {
        const currentAvatar = result[0].avatar;
        if (currentAvatar !== defaultAvatar) {
            const fs = require('fs');
            fs.unlink(avatarPath(currentAvatar), function (err) { });
        }
        connection.query('UPDATE users SET avatar = ? WHERE uid = ?', [avatar, uid]);
    });
};


exports.resetAvatar = (req, res, next) => {
    changeAvatar(req.userData.uid, defaultAvatar);
    res.status(200).json({ message: 'Avatar restored.' });
};


exports.deleteUser = (req, res, next) => {
    const uid = req.userData.uid;
    connection.query('SELECT avatar FROM users WHERE uid = ?', [uid], function (err, result) {
        if (!result.length) {
            next(new Error('User does not exist.'));
            return;
        }
        const avatar = result[0].avatar;
        connection.query('DELETE FROM users WHERE uid = ?', [uid], function (err, result) {
            if (avatar !== defaultAvatar) {
                const fs = require('fs');
                fs.unlink(avatarPath(avatar), function (err) { next(err) });
            }

            res.status(200).json({ message: 'User deleted.' });
        });
    });
};

