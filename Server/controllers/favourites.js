const connection = require('../database/database');


exports.saveFavouriteApartment = (req, res, next) => {
    const uid = req.userData.uid;
    const lodgingId = req.body.lodgingId;
    connection.query('INSERT INTO favourites (uid, apartment_id) VALUES(?, ?)', [uid, lodgingId], function (error, result) {
        res.status(201).json({ message: 'Favourite apartment saved.' });
    });
};


exports.listFavouriteApartments = (req, res, next) => {
    const uid = req.userData.uid;
    const sql = 'SELECT l.id, l.title, l.monthly_rental, l.caution_money, f.date as saved_at, l.main_image FROM favourites f INNER JOIN lodgings l ON f.apartment_id = l.id WHERE f.uid = ? ORDER BY f.date DESC';
    connection.query(sql, [uid], function (err, result) {
        res.status(200).json(result);
    });
};


exports.deleteFromFavourites = (req, res, next) => {
    const uid = req.userData.uid;
    const lodgingId = req.body.lodgingId;
    connection.query('DELETE FROM favourites WHERE uid = ? AND apartment_id = ?', [uid, lodgingId], function (err, result) {
        res.status(200).json({ message: 'Item deleted.' });
    });
};
