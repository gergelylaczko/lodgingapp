const connection = require('../database/database');
const imageUtils = require('../multer/image-utils');


exports.toggleLodgingState = (req, res, next) => {
    const { lodgingId } = req.body;
    connection.query('SELECT status FROM lodgings WHERE id = ?', [lodgingId], function (err, result) {
        const nextStatus = result[0].status === 1 ? 2 : 1;
        connection.query('UPDATE lodgings SET status = ? WHERE id = ?', [nextStatus, lodgingId], function (err, result) {
            res.status(200).json({ message: 'Status updated.' });
        });
    });
};

exports.deleteLodging = (req, res, next) => {
    const { lodgingId } = req.body;
    connection.query('SELECT path AS filename FROM photos WHERE lodging_id = ?', [lodgingId], function (err, result) {
        if (err) { next(err); return; };
        const files = result;
        connection.query('DELETE FROM lodgings WHERE id = ?', [lodgingId], function (err, result) {
            imageUtils.removeUploaded(files, next);
            res.status(200).json({ message: 'Lodging deleted.' });
        });
    });
};

