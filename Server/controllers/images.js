const connection = require('../database/database');
const imageUtils = require('../multer/image-utils');

const defaultImage = require('../config.json').defaultImage;


exports.deleteImage = (req, res, next) => {
    const { imageId, lodgingId } = req.body;
    connection.query('SELECT p.path, l.main_image AS mainImage FROM photos p INNER JOIN lodgings l ON p.lodging_id = l.id WHERE p.id = ?',
        [imageId], function (err, result) {
            if (!result.length) {
                next(new Error('Image does not exist.'));
                return;
            }
            const { path, mainImage } = result[0];
            connection.query('DELETE FROM photos WHERE id = ?', [imageId], function (err, result) {
                if (path === mainImage) {
                    setNewMainImage(lodgingId);
                }

                const fs = require('fs');
                fs.unlink(imageUtils.getImagePath(path), function (err) {
                    if (err) {
                        next(err);
                        return;
                    }
                    res.status(200).json({ message: 'Image deleted.' });
                });
            });
        });
}

const setNewMainImage = (lodgingId) => {
    connection.query('SELECT path FROM photos WHERE lodging_id = ?', [lodgingId], function (err, result) {
        let nextImage = defaultImage;
        if (result.length) {
            nextImage = result[0].path;
        }
        connection.query('UPDATE lodgings SET main_image = ? WHERE id = ?', [nextImage, lodgingId]);
    });
};


exports.setMainImage = (req, res, next) => {
    const { lodgingId, imageId } = req.body;
    connection.query('SELECT path FROM photos p INNER JOIN lodgings l ON p.lodging_id = l.id WHERE l.id = ? AND p.id = ?',
        [lodgingId, imageId], function (err, result) {
            if (!result.length) {
                next(new Error('Not found.'));
                return;
            }
            connection.query('UPDATE lodgings SET main_image = ? WHERE id = ?', [result[0].path, lodgingId], function (err, result) {
                res.status(200).json({ message: 'Main image updated.' });
            });
        });
};



exports.uploadImages = (req, res, next) => {
    const { lodgingId } = req.body;
    const uid = req.userData.uid;
    const uploadedImages = req.files || [];
    if (!uploadedImages.length) {
        next(new Error('No files uploaded.'));
        return;
    }
    connection.query('SELECT uid FROM lodgings WHERE id = ?', [lodgingId], async function (err, result) {
        if (err) {
            next(err);
            return;
        }
        if (!result.length) {
            next(new Error('Not found.'));
            return;
        }
        if (result[0].uid !== uid) {
            imageUtils.removeUploaded(uploadedImages, next);
            next(new Error('Unauthorized.'));
            return;
        }
        if (await checkIfMaximumImagesExceeded(lodgingId, uploadedImages.length)) {
            imageUtils.removeUploaded(uploadedImages, next);
            next(new Error('Maximum image count exceeded.'));
            return;
        }

        uploadedImages.forEach(item => {
            connection.query('INSERT INTO photos (lodging_id, path) VALUES (?, ?)', [lodgingId, item.filename]);
        });
        res.status(201).json({ message: 'Images saved.' });
    });
};

const checkIfMaximumImagesExceeded = (lodgingId, uploadedLength) => {
    return new Promise(resolve => {
        connection.query('SELECT COUNT(*) AS count FROM photos WHERE lodging_id = ?', [lodgingId],
            function (err, result) {
                const count = result[0].count;
                if (count >= 10 || count + uploadedLength > 10)
                    resolve(true);
                else
                    resolve(false);
            });
    });
};
