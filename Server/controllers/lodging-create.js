const connection = require('../database/database');
const defaultImage = require('../config.json').defaultImage;
const imageUtils = require('../multer/image-utils');


exports.createLodging = async (req, res, next) => {
    const uid = req.userData.uid;
    const { city, mainImage } = req.body;
    const uploadedImages = req.files;

    if (!city) {
        imageUtils.removeUploaded(uploadedImages, next);
        next(new Error('Undefined city.'));
        return;
    }
    let data = await prepareParams(req.body, city, uid);
    data = [...data, defaultImage];

    saveLodging(data, uid)
        .then(lodgingId => {
            if (uploadedImages && uploadedImages.length) {
                savePhotos(uploadedImages, lodgingId);
                setMainPhoto(uploadedImages, mainImage, lodgingId);
            }
            res.status(201).json({ message: 'Lodging saved.' });
        })
        .catch(err => { imageUtils.removeUploaded(uploadedImages, next); next(new Error(err.sqlMessage)); });
};

const prepareParams = async (body, city, uid) => {
    const cityId = await getCityId(city);
    const data = Object.assign({}, body, { city: cityId });

    return [data.title, data.size, data.rooms, data.monthlyRental, data.monthlyOverhead, data.cautionMoney, data.readyToMove,
    data.minRentalTime, data.city, data.street, data.houseNumber, data.floor, data.door, data.description, data.typeId,
    data.conditionId, data.elevator, data.heatingId, data.furniture, data.balcony, data.yard, data.pet, data.smoking, data.parkingId,
        uid];
}

const savePhotos = (images, lodgingId) => {
    images.forEach(item => {
        connection.query('INSERT INTO photos (lodging_id, path) VALUES (?, ?)', [lodgingId, item.filename]);
    });
};

const setMainPhoto = (images, mainImage, lodgingId) => {
    let path = images[0].filename;
    if (mainImage) {
        path = images.filter(val => val.originalname === mainImage)[0].filename;
    }
    connection.query('UPDATE lodgings SET main_image = ? WHERE id = ?', [path, lodgingId]);
};

const saveLodging = (params, uid) => {
    const sql = 'INSERT INTO `lodgings`(`title`, `size`, `rooms`, `monthly_rental`, `monthly_overhead`, `caution_money`, `ready_to_move`, `min_rental_time`, `city`, `street`, `house_number`, `floor`, `door`, `description`, `type`, `condition_type`, `elevator`, `heating`, `furniture`, `balcony`, `yard`, `pet`, `smoking`, `parking`, `uid`, `main_image`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

    return new Promise((resolve, reject) => {
        connection.query(sql, params, function (err, result) {
            try {
                if (err) throw err;
                resolve(result.insertId);
            } catch (err) {
                reject(err);
            }
        });
    });
}

const getCityId = (city) => {
    return new Promise((resolve) => {
        connection.query('SELECT id FROM cities WHERE UPPER(name) = UPPER(?)', [city], function (err, result) {
            if (!result.length) {
                const formattedName = city.split(' ').map(word => word[0].toUpperCase() + word.slice(1).toLowerCase()).join(' ');
                connection.query('INSERT INTO cities (name) VALUES (?)', [formattedName], function (err, result) {
                    resolve(result.insertId);
                });
            }
            else {
                resolve(result[0].id);
            }
        });
    });
}

exports.updateLodging = async (req, res, next) => {
    const uid = req.userData.uid;
    const { city, lodgingId } = req.body;

    if (!city) {
        next(new Error('Undefined city.'));
        return;
    }

    let params = await prepareParams(req.body, city, uid);
    params = [...params, lodgingId];

    const sql = 'UPDATE `lodgings` SET `title`= ?,`size`= ?,`rooms`= ?,`monthly_rental`= ?,`monthly_overhead`= ?,`caution_money`= ?,`ready_to_move`= ?,`min_rental_time`= ?,`city`= ?,`street`= ?,`house_number`= ?,`floor`= ?,`door`= ?,`description`= ?,`type`= ?,`condition_type`= ?,`elevator`= ?,`heating`= ?,`furniture`= ?,`balcony`= ?,`yard`= ?,`pet`= ?,`smoking`= ?,`parking`= ?, `uid`= ? WHERE id = ?';
    connection.query(sql, params, function (err, result) {
        if (err) { next(err); return; }
        res.status(201).json({ message: 'Lodging updated.' });
    });
};
