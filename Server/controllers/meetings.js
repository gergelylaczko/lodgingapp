const connection = require('../database/database');


exports.listMeetings = (req, res, next) => {
    const uid = req.userData.uid;
    const sql = 'SELECT l.id, l.title, l.monthly_rental, l.caution_money, m.date AS saved_at, l.main_image FROM meetings m INNER JOIN lodgings l ON m.apartment_id = l.id WHERE m.uid = ? ORDER BY m.date DESC';
    connection.query(sql, [uid], function (err, result) {
        res.status(200).json(result);
    });
};

exports.saveMeeting = (req, res, next) => {
    const uid = req.userData.uid;
    const { lodgingId } = req.body;
    connection.query('INSERT INTO meetings (uid, apartment_id) VALUES (?, ?)', [uid, lodgingId], function (err, result) {
        res.status(201).json({ message: 'Meeting saved.' });
    });
};

exports.deleteMeeting = (req, res, next) => {
    const uid = req.userData.uid;
    const { lodgingId } = req.body;
    connection.query('DELETE FROM meetings WHERE uid = ? AND apartment_id = ?', [uid, lodgingId], function (err, result) {
        res.status(200).json({ message: 'Meeting deleted.' });
    });
};

