const connection = require('../database/database');


exports.listByCities = async (req, res, next) => {
    const city = req.params.city;
    const uid = req.userData.uid;
    const sql = "SELECT l.id, l.title, l.monthly_rental, l.monthly_overhead, l.ready_to_move, main_image AS image, c.name AS city, f.date AS favourite, m.date AS meeting, l.status FROM lodgings l INNER JOIN cities c ON l.city = c.id LEFT OUTER JOIN (SELECT date, apartment_id FROM favourites WHERE uid = ?) f ON l.id = f.apartment_id LEFT OUTER JOIN (SELECT date, apartment_id FROM meetings WHERE uid = ?) m ON m.apartment_id = l.id WHERE l.status = 1 AND UPPER(c.name) LIKE CONCAT('%', UPPER(?), '%') ORDER BY l.date DESC;";
    const params = [uid, uid, city];

    const items = await listQuery(sql, params);
    res.status(200).json(items);
};

exports.listByFilter = async (req, res, next) => {
    const uid = req.userData.uid;
    const body = { city, minSize, maxSize, minRooms, maxRooms, minRental, maxRental, minOverhead, maxOverhead } = req.body;
    const sql = "SELECT l.id, l.title, l.monthly_rental, l.monthly_overhead, l.ready_to_move, main_image AS image, c.name AS city, f.date AS favourite, m.date AS meeting, l.status FROM lodgings l INNER JOIN cities c ON l.city = c.id LEFT OUTER JOIN (SELECT date, apartment_id FROM favourites WHERE uid = ?) f ON l.id = f.apartment_id LEFT OUTER JOIN (SELECT date, apartment_id FROM meetings WHERE uid = ?) m ON m.apartment_id = l.id WHERE l.status = 1 AND UPPER(c.name) LIKE CONCAT('%', UPPER(?), '%') AND size BETWEEN ? AND ? AND rooms BETWEEN ? AND ? AND monthly_rental BETWEEN ? AND ? AND monthly_overhead BETWEEN ? AND ? ORDER BY l.date DESC";
    const params = [uid, uid, ...Object.values(body)];

    const items = await listQuery(sql, params);
    res.status(200).json(items);
};

const listQuery = (sql, params) => {
    return new Promise(resolve => {
        connection.query(sql, params, function (err, result) {
            const items = result.map(item => Object.assign({}, item,
                { favourite: !!item.favourite },
                { meeting: !!item.meeting }
            ));
            resolve(items);
        });
    });
};


exports.lodgingDetails = (req, res, next) => {
    const id = req.params.id;
    const uid = req.userData.uid;
    const sql = 'SELECT l.id, l.title, l.size, l.rooms, l.monthly_rental, l.monthly_overhead, l.caution_money, l.ready_to_move, l.min_rental_time, c.name as city, l.street, l.house_number, l.floor, l.door, l.description, t.type as type, con.name as condition_type, l.elevator, h.name as heating, l.furniture, l.balcony, l.yard, l.pet, l.smoking, p.name AS parking, l.date, u.uid AS owner, u.email, u.first_name, u.last_name, u.phone, s.name AS status, f.favourite, m.meeting FROM lodgings l INNER JOIN cities c ON l.city = c.id INNER JOIN conditions con ON l.condition_type = con.id INNER JOIN heating_types h ON l.heating = h.id INNER JOIN parking_types p ON l.parking = p.id INNER JOIN types t ON l.type = t.id INNER JOIN users u ON l.uid = u.uid INNER JOIN rent_status s ON l.status = s.id LEFT OUTER JOIN (SELECT apartment_id, date AS favourite FROM favourites WHERE uid = ? AND apartment_id = ?) f ON l.id = f.apartment_id LEFT OUTER JOIN (SELECT apartment_id, date AS meeting FROM meetings WHERE uid = ? AND apartment_id = ?) m ON m.apartment_id = l.id WHERE l.id = ?';
    connection.query(sql, [uid, id, uid, id, id], async function (err, result) {
        if (!result.length) {
            next(new Error('Not found.'));
            return;
        }
        const images = await getImagesByLodging(id);
        res.status(200).json({
            ...result[0],
            owner: result[0].owner === uid,
            favourite: !!result[0].favourite,
            meeting: !!result[0].meeting,
            images
        });
    });
}

const getImagesByLodging = (lodging) => {
    return new Promise(resolve => {
        connection.query('SELECT id, path FROM photos WHERE lodging_id = ? ORDER BY id', [lodging], function (err, result) {
            resolve(result);
        });
    });
}

exports.listCities = (req, res, next) => {
    connection.query('SELECT * FROM cities ORDER BY name', function (err, result) {
        res.status(200).json(result);
    });
};

exports.listOwnLodgings = (req, res, next) => {
    const uid = req.userData.uid;
    const sql = 'SELECT l.id, l.title, l.monthly_rental, l.main_image AS image, c.name AS city, s.name AS status FROM lodgings l INNER JOIN cities c ON l.city = c.id INNER JOIN rent_status s ON l.status = s.id WHERE l.uid = ? ORDER BY l.date DESC';
    connection.query(sql, [uid], function (err, result) {
        res.status(200).json(result);
    });
};
