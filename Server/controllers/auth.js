const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const connection = require('../database/database');
const config = require('../config.json');



exports.register = (req, res, next) => {
    const avatar = req.file ? req.file.filename : config.defaultAvatar;
    const { email, firstName, lastName, phone, password } = req.body;

    connection.query('SELECT email FROM users WHERE email = ?', [email], function (err, result) {
        if (result.length) {
            next(new Error('Email address is already in use.'));
            return;
        }
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                const sql = 'INSERT INTO users (email, first_name, last_name, phone, avatar, password) VALUES (?, ?, ?, ?, ?, ?)';
                connection.query(sql, [email, firstName, lastName, phone, avatar, hash], function (err, result) {
                    try {
                        if (err) throw err;
                        res.status(201).json({ message: 'User created.' });
                    } catch (err) {
                        next(err);
                    }
                });
            });
        });
    });
};


exports.login = (req, res, next) => {
    const { email, password } = req.body;
    const sql = 'SELECT uid, first_name, last_name, avatar, password FROM users WHERE email = ?';
    connection.query(sql, [email], function (err, result) {
        if (!result.length) {
            next(new Error('User not found.'));
            return;
        }
        bcrypt.compare(password, result[0].password, function (err, success) {
            if (!success) {
                next(new Error('Invalid email or password.'));
                return;
            }
            const loginSession = `${config.loginSession}h`;
            jwt.sign({ uid: result[0].uid, email }, process.env.JWT_KEY, { expiresIn: loginSession }, function (err, token) {
                res.status(200).json({
                    uid: result[0].uid,
                    firstName: result[0].first_name,
                    lastName: result[0].last_name,
                    avatar: result[0].avatar,
                    email,
                    token
                });
            });
        });
    });
};


exports.checkLogin = (req, res, next) => {
    const uid = req.userData.uid;
    connection.query('SELECT uid, email, first_name, last_name, phone, avatar FROM users WHERE uid = ?', [uid], function (err, result) {
        res.status(200).json({ ...result[0] });
    });
};


exports.changePassword = (req, res, next) => {
    const password = req.body.password;
    const uid = req.userData.uid;
    bcrypt.genSalt(10, function (error, salt) {
        bcrypt.hash(password, salt, function (error, hash) {
            connection.query('UPDATE users SET password = ? WHERE uid = ?', [hash, uid], function (err, result) {
                res.status(201).json({ message: 'New password saved.' });
            });
        });
    });
};

