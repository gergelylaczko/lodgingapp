const connection = require('../database/database');


const simpleQuery = (sql, res) => {
    connection.query(sql, function (err, result) {
        res.status(200).json(result);
    });
}

exports.listApartmentTypes = (req, res, next) => simpleQuery('SELECT * FROM types ORDER BY id', res);
exports.listApartmentConditions = (req, res, next) => simpleQuery('SELECT * FROM conditions ORDER BY id', res);
exports.listApartmentHeatings = (req, res, next) => simpleQuery('SELECT * FROM heating_types ORDER BY id', res);
exports.listParkingTypes = (req, res, next) => simpleQuery('SELECT * FROM parking_types ORDER BY id', res);
