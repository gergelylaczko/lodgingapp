require('dotenv').config();
const config = require('./config.json');
const http = require('http');
const app = require('./app');


const server = http.createServer(app);

const port = config.devPort;
server.listen(port, () => console.log(`Server is running on port: ${port}.`));

