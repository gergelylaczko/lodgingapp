const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



const corsMiddleware = require('./middlewares/cors');
app.use(corsMiddleware.addCors);


const authRoutes = require('./routes/auth');
const lodgingRoutes = require('./routes/lodging');
const favouritesRoutes = require('./routes/favourites');
const userProfileRoutes = require('./routes/user-profile');
const lodgingCreateRoutes = require('./routes/lodging-create');
const lodgingEditRoutes = require('./routes/lodging-edit');
const imagesRoutes = require('./routes/images');
const typesDataRoutes = require('./routes/types-data');
const meetingsRoutes = require('./routes/meetings');

app.use('/auth', authRoutes);
app.use('/lodging', lodgingRoutes);
app.use('/favourites', favouritesRoutes);
app.use('/user', userProfileRoutes);
app.use('/create', lodgingCreateRoutes);
app.use('/edit', lodgingEditRoutes);
app.use('/image', imagesRoutes);
app.use('/types', typesDataRoutes);
app.use('/meetings', meetingsRoutes);


app.use('/uploads', express.static('uploads'));


app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;
