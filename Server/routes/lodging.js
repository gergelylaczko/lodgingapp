const express = require('express');
const router = express.Router();


const authMiddleware = require('../middlewares/check-auth');
const lodgingController = require('../controllers/lodging');


router.get('/list/:city', authMiddleware, lodgingController.listByCities);
router.get('/details/:id', authMiddleware, lodgingController.lodgingDetails);
router.get('/cities', authMiddleware, lodgingController.listCities);
router.get('/own', authMiddleware, lodgingController.listOwnLodgings);
router.post('/filter', authMiddleware, lodgingController.listByFilter);


module.exports = router;
