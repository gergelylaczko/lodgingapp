const express = require('express');
const router = express.Router();


const authMiddleware = require('../middlewares/check-auth');
const favouritesController = require('../controllers/favourites');


router.post('/add', authMiddleware, favouritesController.saveFavouriteApartment);
router.get('/list', authMiddleware, favouritesController.listFavouriteApartments);
router.post('/remove', authMiddleware, favouritesController.deleteFromFavourites);


module.exports = router;
