const express = require('express');
const router = express.Router();
const uploadAvatar = require('../multer/upload-image')('avatars', 10);


const authController = require('../controllers/auth');
const authMiddleware = require('../middlewares/check-auth');


router.post('/register', uploadAvatar.single('avatar'), authController.register);
router.post('/login', authController.login);
router.post('/check', authMiddleware, authController.checkLogin);
router.post('/change', authMiddleware, authController.changePassword);



module.exports = router;

