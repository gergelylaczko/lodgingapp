const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/check-auth');
const typesDataController = require('../controllers/types-data');


router.get('/types', authMiddleware, typesDataController.listApartmentTypes);
router.get('/conditions', authMiddleware, typesDataController.listApartmentConditions);
router.get('/heating-types', authMiddleware, typesDataController.listApartmentHeatings);
router.get('/parking-types', authMiddleware, typesDataController.listParkingTypes);




module.exports = router;
