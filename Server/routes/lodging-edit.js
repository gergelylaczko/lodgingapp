const express = require('express');
const router = express.Router();

const checkAuth = require('../middlewares/check-auth');
const lodgingProtection = require('../middlewares/lodging-protection');
const lodgingEditController = require('../controllers/lodging-edit');


router.post('/status', checkAuth, lodgingProtection, lodgingEditController.toggleLodgingState);
router.post('/delete', checkAuth, lodgingProtection, lodgingEditController.deleteLodging);



module.exports = router;
