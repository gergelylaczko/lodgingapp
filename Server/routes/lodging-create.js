const express = require('express');
const router = express.Router();
const uploadImages = require('../multer/upload-image')('images', 20);

const authMiddleware = require('../middlewares/check-auth');
const lodgingProtection = require('../middlewares/lodging-protection');
const lodgingCreateController = require('../controllers/lodging-create');

router.post('/add', authMiddleware, uploadImages.array('photos', 10), lodgingCreateController.createLodging);
router.post('/update', authMiddleware, lodgingProtection, lodgingCreateController.updateLodging);


module.exports = router;
