const express = require('express');
const router = express.Router();
const uploadImage = require('../multer/upload-image')('images', 20);

const checkAuth = require('../middlewares/check-auth');
const lodgingProtection = require('../middlewares/lodging-protection');
const imagesController = require('../controllers/images');


router.post('/delete', checkAuth, lodgingProtection, imagesController.deleteImage);
router.post('/set-main', checkAuth, lodgingProtection, imagesController.setMainImage);
router.post('/upload', checkAuth, uploadImage.array('photos', 10), imagesController.uploadImages);



module.exports = router;
