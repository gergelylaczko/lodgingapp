const express = require('express');
const router = express.Router();
const uploadAvatar = require('../multer/upload-image')('avatars', 10);


const authMiddleware = require('../middlewares/check-auth');
const userProfileController = require('../controllers/user-profile');


router.get('/profile', authMiddleware, userProfileController.userDetails);
router.post('/update', authMiddleware, uploadAvatar.single('avatar'), userProfileController.updateUser);
router.post('/reset-avatar', authMiddleware, userProfileController.resetAvatar);
router.post('/delete', authMiddleware, userProfileController.deleteUser);


module.exports = router; 
