const express = require('express');
const router = express.Router();

const checkAuth = require('../middlewares/check-auth');
const meetingsController = require('../controllers/meetings');


router.get('/list', checkAuth, meetingsController.listMeetings);
router.post('/add', checkAuth, meetingsController.saveMeeting);
router.post('/delete', checkAuth, meetingsController.deleteMeeting);


module.exports = router;
