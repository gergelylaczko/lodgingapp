const connection = require('../database/database');

module.exports = (req, res, next) => {
    const { lodgingId } = req.body;
    const uid = req.userData.uid;
    connection.query('SELECT uid FROM lodgings WHERE id = ?', [lodgingId], function (err, result) {
        if (err) {
            next(err);
            return;
        }
        if (!result.length) {
            next(new Error('Not found.'));
            return;
        }
        if (result[0].uid !== uid) {
            next(new Error('Unauthorized.'));
            return;
        }
        next();
    });
};