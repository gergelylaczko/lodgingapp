
const getImagePath = (filename) => `uploads/images/${filename}`;

exports.getImagePath = getImagePath;

exports.removeUploaded = (files, next) => {
    const fs = require('fs');
    if (files) {
        files.forEach(file => {
            fs.unlink(getImagePath(file.filename), function (err) { next(err) });
        });
    }
};

