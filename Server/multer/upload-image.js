const multer = require('multer');
const path = require('path');


const prepareUpload = (savePath, maxSize) => {

    const storage = multer.diskStorage({
        destination: function (req, file, callBack) {
            callBack(null, `./uploads/${savePath}`);
        },
        filename: function (req, file, callBack) {
            callBack(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
        }
    });

    const upload = multer({
        storage: storage,
        limits: {
            fieldSize: 1024 * 1024 * maxSize
        },
        fileFilter: function (req, file, callBack) {
            const ext = path.extname(file.originalname);
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg')
                return callBack(new Error('Only images allowed!'), false);
            callBack(null, true);
        }
    });

    return upload;
}


module.exports = prepareUpload;
